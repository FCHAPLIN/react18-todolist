import PropTypes from "prop-types";

function TodoList(props) {
  const { todos } = props;
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>{/* TODO show the list here */}</ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
